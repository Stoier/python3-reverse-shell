#!/usr/bin/python

import socket
from termcolor import colored

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

s.bind(("192.168.1.87", 54321))
s.listen(5)

print(colored("[+] Listening For Incoming Connection", 'blue'))

target, ip = s.accept()
print(colored(f"[+] Connection Established From: {str(ip)}", 'green'))
s.close()